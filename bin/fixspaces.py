#!/usr/bin/env python3

import tkinter as tk

class TextTool(tk.Tk):

    def __init__(self):
        super().__init__()
        self.title("TextTool para Luciana")
        self.geometry("900x700")
        self.src = tk.Text()
        self.dst = tk.Text()
        self.replace = tk.Button(self, text="Apply and replace ↓", command=self.replace, width=20, height=1)
        self.append = tk.Button(self, text="Apply and append ↓", command=self.append, width=20, height=1)
        self.clipboard_paste = tk.Button(self, text="Paste from clipboard ↑", command=self.paste_from_clipboard, width=20, height=1)
        self.clipboard_copy = tk.Button(self, text="Copy to clipboard ⎘", command=self.copy_to_clipboard, width=20, height=1)
        self.src.config(width=100, height=15)
        self.src.place(relx=0.5, y=30, anchor=tk.N)
        self.clipboard_paste.place(relx=0.2, y=300, anchor=tk.N)
        self.replace.place(relx=0.5, y=300, anchor=tk.N)
        self.append.place(relx=0.8, y=300, anchor=tk.N)
        self.clipboard_copy.place(relx=0.5, y=600, anchor=tk.N)
        self.dst.config(width=100, height=15)
        self.dst.place(relx=0.5, y=350, anchor=tk.N)

    def replace(self):
        self.dst.delete('1.0', "end-1c")
        text = self.src.get(1.0, 'end-1c')
        result = prettify(text)
        self.dst.insert("end-1c", result)

    def append(self):
        text = self.src.get(1.0, 'end-1c')
        result = prettify(text)
        self.dst.insert("end-1c", "\n" + result)

    def copy_to_clipboard(self):
        text = self.dst.get(1.0, 'end-1c')
        self.clipboard_clear()
        self.clipboard_append(text)

    def paste_from_clipboard(self):
        text = self.clipboard_get()
        self.src.delete('1.0', "end-1c")
        self.src.insert("end-1c", text)
   
def prettify(text):
    out = []
    for line in text.split("\n"):
        line = line.strip()
        if not line:
            out.append("\n")
            continue
        if line[-1] == "-":
            line = line[:-1]
            out.append(line)
        else:
            out.append(line + " ")
    return "".join(out)

if __name__ == "__main__":
    texttool = TextTool()
    texttool.mainloop()
