#!/usr/bin/env python3

import sys
import mido
from mido import MidiFile, MidiTrack

def trim_leading_silence(midi_file_path, output_file_path):
    # Load the MIDI file
    midi = MidiFile(midi_file_path)

    # Create a new MIDI file to store the trimmed version
    trimmed_midi = MidiFile(ticks_per_beat=midi.ticks_per_beat)  # Preserve ticks per beat
    trimmed_track = MidiTrack()
    trimmed_midi.tracks.append(trimmed_track)

    # Variables to track time and the first note
    first_note_found = False
    time_elapsed = 0
    time_elapsed_at_first_note = 0

    # Iterate through all tracks and messages to find the first note
    for track in midi.tracks:
        for msg in track:
            # Accumulate time from previous messages
            time_elapsed += msg.time

            # If the message is a note_on event with velocity > 0, it's the first note
            if msg.type == 'note_on' and msg.velocity > 0:
                first_note_found = True
                time_elapsed_at_first_note = time_elapsed
                break

        if first_note_found:
            break

    # If no note was found, just return the original file
    if not first_note_found:
        print("No notes found in the MIDI file.")
        return

    # Reset time_elapsed and iterate again to copy messages after the first note
    time_elapsed = 0
    for track in midi.tracks:
        for msg in track:
            time_elapsed += msg.time

            # Only add messages that occur after the first note
            if time_elapsed >= time_elapsed_at_first_note:
                # Adjust the time of the first message after trimming
                if time_elapsed == time_elapsed_at_first_note:
                    # Set the time of the first message to 0
                    trimmed_track.append(msg.copy(time=0))
                else:
                    # Copy the message with its original time
                    trimmed_track.append(msg.copy(time=msg.time))

    # Save the trimmed MIDI file
    trimmed_midi.save(output_file_path)
    print(f"Trimmed MIDI file saved as {output_file_path}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python trim_midi.py <input_midi_file>")
        sys.exit(1)

    input_midi_file = sys.argv[1]
    output_midi_file = input_midi_file.replace('.mid', '-trim.mid')

    trim_leading_silence(input_midi_file, output_midi_file)
