# How to use this Flancian

If you found this in a repository called 'bin', you are looking at an old version. The new version is [[chezmoi]] managed (instead of managed directly with git). The canonical location for this repository as of 2023-02 is https://gitlab.com/flancian/flancian.

For my other repositories, see https://gitlab.com/flancian and https://github.com/flancian.

This Flancian shall be used for good and not for evil.

May you be happy!
