#!/bin/bash

#!/bin/bash

# Set paths
SOUNDFONT=~/Yamaha\ C5\ Grand-v2.4.sf2  # Adjust if needed
OUTPUT_DIR="./rendered"

# Create output directory if not exists
mkdir -p "$OUTPUT_DIR"

# Batch process MIDI files
for midi in *.mid; do
    base_name=$(basename "$midi" .mid)
    output_wav="$OUTPUT_DIR/${base_name}.wav"

    echo "Rendering: $midi -> $output_wav"

    # FluidSynth render with higher volume
    fluidsynth -ni "$SOUNDFONT" "$midi" -F "$output_wav" -r 44100 -o synth.gain=0.3

    # Optional: Normalize volume to prevent clipping
    sox "$output_wav" "$OUTPUT_DIR/${base_name}_normalized.wav" gain -n -3

    # Optional: Convert to MP3
    ffmpeg -i "$OUTPUT_DIR/${base_name}_normalized.wav" -b:a 320k "$OUTPUT_DIR/${base_name}.mp3"

    # Optional: Convert to Opus
    ffmpeg -i "$OUTPUT_DIR/${base_name}_normalized.wav" -c:a libopus -b:a 128k "$OUTPUT_DIR/${base_name}.opus"

    echo "Finished: $midi"
done

echo "All MIDI files rendered!"
