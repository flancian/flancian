#!/usr/bin/env python3

import tkinter as tk
import ipdb

class TextTool(tk.Tk):

    def __init__(self):
        super().__init__()
        self.title("TextTool para Luciana")
        self.geometry("900x900")
        self.src = tk.Text()
        self.dst = tk.Text()
        self.replace = tk.Button(self, text="Apply and replace", command=self.replace, width=20, height=1)
        self.append = tk.Button(self, text="Apply and append", command=self.append, width=20, height=1)
        self.clipboard = tk.Button(self, text="Copy to clipboard", command=self.copy_to_clipboard, width=20, height=1)
        self.src.config(width=100, height=20)
        self.src.place(relx=0.5, y=50, anchor=tk.N)
        self.replace.place(relx=0.3, y=400, anchor=tk.N)
        self.append.place(relx=0.7, y=400, anchor=tk.N)
        self.clipboard.place(relx=0.5, y=850, anchor=tk.N)
        self.dst.config(width=100, height=20)
        self.dst.place(relx=0.5, y=475, anchor=tk.N)

    def replace(self):
        self.dst.delete('1.0', "end-1c")
        text = self.src.get(1.0, 'end-1c')
        result = prettify(text)
        self.dst.insert("end-1c", result)

    def append(self):
        text = self.src.get(1.0, 'end-1c')
        result = prettify(text)
        self.dst.insert("end-1c", "\n" + result)

    def copy_to_clipboard(self):
        text = self.dst.get(1.0, 'end-1c')
        self.clipboard_clear()
        self.clipboard_append(text)
   
def prettify(text):
    out = []
    for line in text.split("\n"):
        line = line.strip()
        if line[-1] == "-":
            line = line[:-1]
            out.append(line)
        else:
            out.append(line + " ")
        print(out)
    return "".join(out)

if __name__ == "__main__":
    texttool = TextTool()
    texttool.mainloop()
