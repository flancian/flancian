# Flancian

Instructions and tools to run a [[Flancian]], meaning https://anagora.org/flancian :)

## Getting started

This is currently (as of [[2023-03-18]]) a set of links to repositories which I find personally relevant, plus some instructions to build and run some associated  processes.

This README in particular and this repository in general assumes you have access to an Agora, in particular the [Agora of Flancia](https://anagora.org). If an Agora is not available to you as of the time of reading, you should be able to bootstrap one with the instructions found here and in the Agora of Flancia's root repository.

## Projects and repositories

- [Flancia](https://flancia.org):
  - https://anagora.org/go/flancia/git
- [An Agora](https://anagora.org): 
  - https://anagora.org/go/agora (root repository)
  - https://anagora.org/go/agora-server (interlay, UI)
  - https://anagora.org/go/agora-bridge (importer, bots)
- My digital garden (part of the above):
  - https://anagora.org/go/flancian/garden
- My social feeds:
  - https://twitter.com/flancian
  - https://social.coop/@flancian
- My dotfiles and local bin:
  - Using Chezmoi: https://gitlab.com/flancian/flancian

See [Dockerfile](https://anagora.org/go/flancian/Dockerfile) for a maybe working container definition that should pull all of the above and make it work (tm) for you -- provided you respect the license :)

## License

This Flancian shall be used for Good, and not for Evil -- as defined by the Agora of Flancia.
